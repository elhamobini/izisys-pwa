import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({
    rtl: true,
    theme: {
        options: {
            customProperties: true,
        },
        themes: {
            light: {
                primary: '#141414',
                secondary: '#2D2D2D',
                accent: '#DCB067',
                fourth: '#fff',
                error: '#FF5252',
                info: '#2196F3',
                success: '#4CAF50',
                warning: '#FFC107'
            },
        },
    },
    icons: {
        iconfont: 'fa',
    },
});
