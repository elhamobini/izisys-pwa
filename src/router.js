import Vue from 'vue'
import VueRouter from 'vue-router'
import Router from 'vue-router'
import Home from "./components/Home";
import CurrentDetail from "./components/CurrentDetail";
import AllProductDetail from "./components/AllProductDetail";
import Services from "./components/Services";
import CurrentProjects from "./components/CurrentProjects";
import AllProducts from "./components/AllProducts";
import ContactUs from "./components/ContactUs";
import AboutUs from "./components/AboutUs";
import WebsiteDetail from "./components/WebsiteDetail";

Vue.use(VueRouter);
export default new Router({
    routes: [
        {
            path: '/',
            redirect: '/Home'
        },
        {
            path: "/Home",
            component: Home,
        },
        {
            path: "/CurrentDetail",
            component: CurrentDetail,
        },
        {
            path: "/AllProductDetail",
            component: AllProductDetail,
        },
        {
            path: "/Services",
            component: Services,
        },
        {
            path: "/CurrentProjects",
            component: CurrentProjects,
        },
        {
          path: "/AllProducts",
          component: AllProducts,
        },
        {
            path: "/ContactUs",
            component: ContactUs,
        },
        {
            path: "/AboutUs",
            component: AboutUs,
        },
        {
          path: "/WebsiteDetail"  ,
            component: WebsiteDetail,
        },
    ]
});
