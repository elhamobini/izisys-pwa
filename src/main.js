import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import router from "./router";
import moment from "moment-jalaali";
import './registerServiceWorker'
import VueSocialSharing from 'vue-social-sharing'


import "@/assets/styles/master.css";
import "@/assets/styles/bootstrap.min.css";
import "@/assets/styles/fontawesome.min.css";
import "@/assets/styles/brands.css";
import "@/assets/styles/light.min.css";
import "@/assets/styles/solid.min.css";
import "@/assets/styles/vuetify.css";




Vue.config.productionTip = false
Vue.use(VueSocialSharing);
import VueGeolocation from 'vue-browser-geolocation'
Vue.use(VueGeolocation)

import * as VueGoogleMaps from 'vue2-google-maps'
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyBqLXDV8HIaaA2ATo8cFm99FMI75WYFNSg'
  }
})

Vue.prototype.$faJson = null;
Vue.prototype.$getText = function (constant) {
  if (this.$faJson === null) {
    this.$faJson = require("./assets/json/langfa.json");
  }
  return this.$faJson[constant];
}
Vue.prototype.$getCurrentProjects = function () {
    return  require("./assets/json/currentfa.json");
}
Vue.prototype.$getAllProjects = function () {
    return  require("./assets/json/projectsfa.json");
}
Vue.prototype.$getServices = function () {
  return  require("./assets/json/servicesfa.json");
}
Vue.prototype.$getSupports = function () {
  return  require("./assets/json/supportsfa.json");
}
Vue.prototype.$getBenefits = function () {
  return  require("./assets/json/benefitsfa.json");
}
Vue.prototype.$getWebsites = function () {
  return  require("./assets/json/websitesfa.json");
}


/*Vue.prototype.$enJson = null;
Vue.prototype.$getText = function (constant) {
  if (this.$enJson === null) {
    this.$enJson = require("./assets/json/langen.json");
  }
  return this.$enJson[constant];
}*/

Vue.prototype.$convertDate = function (date) {
  let sliceDate = date.slice(0, 10)
  return moment(sliceDate).format('jYYYY/jM/jD')
}
Vue.prototype.$computingPriceOffer = function (price, off) {
  let priceNum = price;

  if (typeof priceNum !== "number") {
    priceNum = price.replace(",", "");
  }
  return this.$numberWithCommas(parseInt(priceNum) - (parseInt(priceNum) * (off / 100)));
};
Vue.prototype.$numberWithCommas = function (x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};
Vue.prototype.$productInfoArray = []

Vue.prototype.$global = {};
new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
